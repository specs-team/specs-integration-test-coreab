# Integration scenarios CoreAB

This family of scenarios integrates components responsible for the SLA negotiation and SLA enforcement phase. More information about integration scenarios can be found in deliverables D1.5.1 and D1.5.2.

### Core-AB1
This scenario integrates the Core-A1 and Core-B1 scenarios. Involved artefacts enable the complete negotiation phase (the basic version without ranking SLA Offers).

Details:
- Base Scenario ID: Core-A1, Core-B1
- Added artefacts: /

Involved components:
- SLA Platform:	SLA Manager, Service Manager
- Negotiation module: SLO Manager, Supply Chain Manager
- Enforcement module: Planning
- Monitoring module: /
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: /

### Core-AB2
This scenario extends the Core-AB1 integration scenario with the Implementation component (Enforcement module) responsible for the acquisition and configuration of cloud resources.

Details:
- Base Scenario ID: Core-AB1
- Added artefacts: Implementation

Involved components:
- SLA Platform:	SLA Manager, Service Manager
- Negotiation module: SLO Manager, Supply Chain Manager
- Enforcement module: Planning, Implementation
- Monitoring module: /
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: /

### Core-AB3
This scenario extends the Core-AB2 integration scenario with the MoniPoli Filter component (Monitoring module) that is configured during the SLA implementation phase and is responsible for the identification of possible SLA alerts and violations.

Details:
- Base Scenario ID: Core-AB2
- Added artefacts: MoniPoli Filter, Event Hub

Involved components:
- SLA Platform:	SLA Manager, Service Manager
- Negotiation module: SLO Manager, Supply Chain Manager
- Enforcement module: Planning, Implementation
- Monitoring module: MoniPoli Filter, Event Hub
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: /

### Core-AB4
This scenario extends the Core-AB3 integration scenario with the default SPECS Application. The involved artifacts enable the basic version of the SPECS flow up to the SLA monitoring phase (SLA negotiation and SLA implementation).

Details:
- Base Scenario ID: Core-AB3
- Added artefacts: Default SPECS Application

Involved components:
- SLA Platform:	SLA Manager, Service Manager
- Negotiation module: SLO Manager, Supply Chain Manager
- Enforcement module: Planning, Implementation
- Monitoring module: MoniPoli Filter, Event Hub
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: Default SPECS Application
