package eu.specs.project.integrationtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.SecurityMechanism;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.integrationtest.model.CollectionType;
import eu.specs.project.integrationtest.utils.AppConfig;
import eu.specs.project.integrationtest.utils.JsonDumper;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;

public class IntegrationScenarioCoreAB1Test {
    private static final Logger logger = LogManager.getLogger(IntegrationScenarioCoreAB1Test.class);
    private static final String SLA_TEMPLATE_FILE = "SLATemplate-WebPool.xml";
    private static final String[] MECHANISM_FILES = {"mechanism-WebPool.json"};
    private static int OFFER_NUM_TO_ACCEPT = 4;
    private AppConfig appConfig;

    @Before
    public void setUp() throws Exception {
        appConfig = new AppConfig("/app.properties");
    }

    @Test
    public void test() throws Exception {
        System.out.println("Integration scenario Core AB1 test started.");
        System.out.println("Using SPECS platform: " + appConfig.getSpecsPlatformIp());
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Client client = ClientBuilder.newBuilder()
                    .register(JacksonJsonProvider.class)
                            //.register(new LoggingFilter())
                    .build();
            WebTarget sloManagerTarget = client.target(appConfig.getSloManagerAddress());
            WebTarget serviceManagerTarget = client.target(appConfig.getServiceManagerApiAddress());
            WebTarget slaManagerTarget = client.target(appConfig.getSlaManagerApiAddress());
            WebTarget planningTarget = client.target(appConfig.getPlanningApiAddress());

            String slaTemplateXml = IOUtils.toString(
                    this.getClass().getClassLoader().getResourceAsStream(SLA_TEMPLATE_FILE));

            JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
            Unmarshaller u = jaxbContext.createUnmarshaller();
            AgreementOffer slaTemplate = (AgreementOffer) u.unmarshal(new StringReader(slaTemplateXml));

            for (String securityMechanismFile : MECHANISM_FILES) {
                SecurityMechanism securityMechanism = objectMapper.readValue(
                        this.getClass().getResourceAsStream("/" + securityMechanismFile), SecurityMechanism.class);

                System.out.println("Removing old security mechanism " + securityMechanism.getId());
                serviceManagerTarget
                        .path("/security-mechanisms/{id}")
                        .resolveTemplate("id", securityMechanism.getId())
                        .request()
                        .delete();

                System.out.println("Uploading security mechanism " + securityMechanism.getId());
                serviceManagerTarget
                        .path("/security-mechanisms")
                        .request()
                        .post(Entity.text(objectMapper.writeValueAsString(securityMechanism)));
            }

            System.out.println("Removing old SLA template if it exists...");
            sloManagerTarget
                    .path("/sla-templates/{name}")
                    .resolveTemplate("name", slaTemplate.getName())
                    .request()
                    .delete();

            System.out.println(String.format("Uploading SLA template %s...", slaTemplate.getName()));
            sloManagerTarget
                    .path("/sla-templates")
                    .request()
                    .post(Entity.entity(slaTemplateXml, MediaType.TEXT_XML_TYPE));

            System.out.println("Retrieving custom SLA 1...");
            String customSla1Xml = sloManagerTarget
                    .path("/sla-templates/{name}")
                    .resolveTemplate("name", slaTemplate.getName())
                    .request(MediaType.TEXT_XML)
                    .get(String.class);

            System.out.println("Retrieving custom SLA 2...");
            String customSla2Xml = sloManagerTarget
                    .path("/sla-templates/{name}")
                    .resolveTemplate("name", slaTemplate.getName())
                    .request(MediaType.TEXT_XML)
                    .post(Entity.entity(customSla1Xml, MediaType.TEXT_XML_TYPE), String.class);

            AgreementOffer customSla = (AgreementOffer) u.unmarshal(new StringReader(customSla2Xml));
            System.out.println(String.format("Custom SLA: %s", customSla.getName()));

            String slaId = customSla.getName();

            // get SLA offers
            CollectionType slaOffersCollection = sloManagerTarget
                    .path("/sla-templates/{slatOfferId}/slaoffers")
                    .resolveTemplate("slatOfferId", customSla.getName())
                    .request("text/xml")
                    .post(Entity.entity(customSla2Xml, MediaType.TEXT_XML_TYPE), CollectionType.class);

            System.out.println("SLA offers collection:");
            System.out.println(JsonDumper.dump(slaOffersCollection));

            assertEquals("Invalid number of SLA offers.", slaOffersCollection.getItemList().size(), 4);

            // retrieve supply chains collection
            ResourceCollection supplyChainCollection = planningTarget
                    .path("/supply-chains")
                    .queryParam("slaId", customSla.getName())
                    .request(MediaType.APPLICATION_JSON)
                    .get(ResourceCollection.class);

            System.out.println("Supply chains collection:");
            System.out.println(JsonDumper.dump(supplyChainCollection));
            System.out.println();

            // retrieve supply chains
            for (ResourceCollection.Item item : supplyChainCollection.getItemList()) {
                SupplyChain supplyChain = client
                        .target(item.getItem())
                        .request(MediaType.APPLICATION_JSON)
                        .get(SupplyChain.class);

                System.out.println(String.format("Supply chain %s:", supplyChain.getId()));
                System.out.println(JsonDumper.dump(supplyChain));
                System.out.println();

                assertEquals("Invalid SLA ID.", supplyChain.getSlaId(), slaId);
            }

            // retrieve SLA offer that should be accepted
            System.out.println("SLA offer to accept: " + OFFER_NUM_TO_ACCEPT);
            String acceptedOfferUri = slaOffersCollection.getItemList().get(OFFER_NUM_TO_ACCEPT - 1).getValue();
            System.out.println("Retrieving SLA offer " + acceptedOfferUri);
            String slaOffer1Xml = client
                    .target(acceptedOfferUri)
                    .request(MediaType.TEXT_XML)
                    .get(String.class);
            AgreementOffer slaOffer1 = (AgreementOffer) u.unmarshal(new StringReader(slaOffer1Xml));
            System.out.println(String.format("SLA offer %d (ID=%s)", OFFER_NUM_TO_ACCEPT, slaOffer1.getName()));
            System.out.println();

            // accept one of SLA offers (other SLA offers and supply chains will be deleted)
            String slaXml = sloManagerTarget
                    .path("/sla-templates/{slatOfferId}/slaoffers/current")
                    .resolveTemplate("slatOfferId", customSla.getName())
                    .request(MediaType.APPLICATION_XML_TYPE)
                    .put(Entity.entity(slaOffer1Xml, MediaType.TEXT_XML_TYPE), String.class);

            AgreementOffer sla = (AgreementOffer) u.unmarshal(new StringReader(slaXml));
            System.out.println(String.format("SLA offer %s has been accepted.", slaOffer1.getName()));
            System.out.println("SLA ID: " + sla.getName());
            System.out.println();

            // retrieve supply chains collection
            ResourceCollection supplyChainCollection1 = planningTarget
                    .path("/supply-chains")
                    .queryParam("slaId", customSla.getName())
                    .request(MediaType.APPLICATION_JSON)
                    .get(ResourceCollection.class);

            System.out.println("Supply chains collection:");
            System.out.println(JsonDumper.dump(supplyChainCollection1));
            System.out.println();
            assertEquals("Obsolete supply chains should be deleted.",
                    supplyChainCollection1.getItemList().size(), 1);

            // sign the SLA
            System.out.println(String.format("Setting SLA state to SIGNED..."));
            Response response = slaManagerTarget
                    .path("/slas/{slaId}/sign")
                    .resolveTemplate("slaId", slaId)
                    .request()
                    .post(Entity.entity(slaXml, MediaType.TEXT_XML_TYPE));
            assertEquals(response.getStatusInfo().getFamily(), Response.Status.Family.SUCCESSFUL);

            // retrieve SLA status
            String slaStatus = slaManagerTarget
                    .path("/slas/{slaId}/status")
                    .resolveTemplate("slaId", slaId)
                    .request()
                    .get(String.class);
            assertEquals(slaStatus, "SIGNED");

        } catch (WebApplicationException e) {
            String resp = e.getResponse().readEntity(String.class);
            System.out.println("REST API request failed: " + e.getMessage() + "\n" + resp);
            throw e;
        }

        System.out.println("Integration scenario Core AB1 test finished successfully.");
    }
}
