package eu.specs.project.integrationtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.common.Annotation;
import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.*;
import eu.specs.project.integrationtest.model.CollectionType;
import eu.specs.project.integrationtest.utils.AppConfig;
import eu.specs.project.integrationtest.utils.JsonDumper;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class IntegrationScenarioCoreAB3Test {
    private static final Logger logger = LogManager.getLogger(IntegrationScenarioCoreAB3Test.class);
    private static final String SLA_TEMPLATE_FILE = "SLATemplate-WebPool.xml";
    private static final String[] MECHANISM_FILES = {"mechanism-WebPool.json"};
    private static int OFFER_NUM_TO_ACCEPT = 4;
    private static final int PLANNING_ACTIVITY_TIMEOUT = 1800;
    private AppConfig appConfig;

    @Before
    public void setUp() throws Exception {
        appConfig = new AppConfig("/app.properties");
    }

    @Test
    public void test() throws Exception {
        System.out.println("Integration scenario Core AB3 test started.");
        System.out.println("Using SPECS platform: " + appConfig.getSpecsPlatformIp());
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Client client = ClientBuilder.newBuilder()
                    .register(JacksonJsonProvider.class)
                            //.register(new LoggingFilter())
                    .build();
            WebTarget sloManagerTarget = client.target(appConfig.getSloManagerAddress());
            WebTarget serviceManagerTarget = client.target(appConfig.getServiceManagerApiAddress());
            WebTarget slaManagerTarget = client.target(appConfig.getSlaManagerApiAddress());
            WebTarget planningTarget = client.target(appConfig.getPlanningApiAddress());
            WebTarget implementationTarget = client.target(appConfig.getImplementationApiAddress());
            WebTarget monipoliTarget = client.target(appConfig.getMonipoliAddress());

            String slaTemplateXml = IOUtils.toString(
                    this.getClass().getClassLoader().getResourceAsStream(SLA_TEMPLATE_FILE));

            JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
            Unmarshaller u = jaxbContext.createUnmarshaller();
            AgreementOffer slaTemplate = (AgreementOffer) u.unmarshal(new StringReader(slaTemplateXml));

            for (String securityMechanismFile : MECHANISM_FILES) {
                SecurityMechanism securityMechanism = objectMapper.readValue(
                        this.getClass().getResourceAsStream("/" + securityMechanismFile), SecurityMechanism.class);

                System.out.println("Removing old security mechanism " + securityMechanism.getId());
                serviceManagerTarget
                        .path("/security-mechanisms/{id}")
                        .resolveTemplate("id", securityMechanism.getId())
                        .request()
                        .delete();

                System.out.println("Uploading security mechanism " + securityMechanism.getId());
                serviceManagerTarget
                        .path("/security-mechanisms")
                        .request()
                        .post(Entity.text(objectMapper.writeValueAsString(securityMechanism)));
            }

            System.out.println("Removing old SLA template if it exists...");
            sloManagerTarget
                    .path("/sla-templates/{name}")
                    .resolveTemplate("name", slaTemplate.getName())
                    .request()
                    .delete();

            System.out.println(String.format("Uploading SLA template %s...", slaTemplate.getName()));
            sloManagerTarget
                    .path("/sla-templates")
                    .request()
                    .post(Entity.entity(slaTemplateXml, MediaType.TEXT_XML_TYPE));

            System.out.println("Retrieving custom SLA 1...");
            String customSla1Xml = sloManagerTarget
                    .path("/sla-templates/{name}")
                    .resolveTemplate("name", slaTemplate.getName())
                    .request(MediaType.TEXT_XML)
                    .get(String.class);

            System.out.println("Retrieving custom SLA 2...");
            String customSla2Xml = sloManagerTarget
                    .path("/sla-templates/{name}")
                    .resolveTemplate("name", slaTemplate.getName())
                    .request(MediaType.TEXT_XML)
                    .post(Entity.entity(customSla1Xml, MediaType.TEXT_XML_TYPE), String.class);

            AgreementOffer customSla = (AgreementOffer) u.unmarshal(new StringReader(customSla2Xml));
            System.out.println(String.format("Custom SLA: %s", customSla.getName()));

            String slaId = customSla.getName();

            // get SLA offers
            CollectionType slaOffersCollection = sloManagerTarget
                    .path("/sla-templates/{slatOfferId}/slaoffers")
                    .resolveTemplate("slatOfferId", customSla.getName())
                    .request("text/xml")
                    .post(Entity.entity(customSla2Xml, MediaType.TEXT_XML_TYPE), CollectionType.class);

            System.out.println("SLA offers collection:");
            System.out.println(JsonDumper.dump(slaOffersCollection));

            assertEquals("Invalid number of SLA offers.", slaOffersCollection.getItemList().size(), 4);

            // retrieve supply chains collection
            ResourceCollection supplyChainCollection = planningTarget
                    .path("/supply-chains")
                    .queryParam("slaId", customSla.getName())
                    .request(MediaType.APPLICATION_JSON)
                    .get(ResourceCollection.class);

            System.out.println("Supply chains collection:");
            System.out.println(JsonDumper.dump(supplyChainCollection));
            System.out.println();

            // retrieve supply chains
            for (ResourceCollection.Item item : supplyChainCollection.getItemList()) {
                SupplyChain supplyChain = client
                        .target(item.getItem())
                        .request(MediaType.APPLICATION_JSON)
                        .get(SupplyChain.class);

                System.out.println(String.format("Supply chain %s:", supplyChain.getId()));
                System.out.println(JsonDumper.dump(supplyChain));
                System.out.println();

                assertEquals("Invalid SLA ID.", supplyChain.getSlaId(), slaId);
            }

            // retrieve SLA offer that should be accepted
            System.out.println("SLA offer to accept: " + OFFER_NUM_TO_ACCEPT);
            String acceptedOfferUri = slaOffersCollection.getItemList().get(OFFER_NUM_TO_ACCEPT - 1).getValue();
            System.out.println("Retrieving SLA offer " + acceptedOfferUri);
            String slaOffer1Xml = client
                    .target(acceptedOfferUri)
                    .request(MediaType.TEXT_XML)
                    .get(String.class);
            AgreementOffer slaOffer1 = (AgreementOffer) u.unmarshal(new StringReader(slaOffer1Xml));
            System.out.println(String.format("SLA offer %d (ID=%s)", OFFER_NUM_TO_ACCEPT, slaOffer1.getName()));
            System.out.println();

            // accept one of SLA offers (other SLA offers and supply chains will be deleted)
            String slaXml = sloManagerTarget
                    .path("/sla-templates/{slatOfferId}/slaoffers/current")
                    .resolveTemplate("slatOfferId", customSla.getName())
                    .request(MediaType.APPLICATION_XML_TYPE)
                    .put(Entity.entity(slaOffer1Xml, MediaType.TEXT_XML_TYPE), String.class);

            AgreementOffer sla = (AgreementOffer) u.unmarshal(new StringReader(slaXml));
            System.out.println(String.format("SLA offer %s has been accepted.", slaOffer1.getName()));
            System.out.println();

            // sign the SLA
            System.out.println(String.format("Setting SLA state to SIGNED..."));
            Response response = slaManagerTarget
                    .path("/slas/{slaId}/sign")
                    .resolveTemplate("slaId", slaId)
                    .request()
                    .post(Entity.entity(slaXml, MediaType.TEXT_XML_TYPE));
            assertEquals(response.getStatusInfo().getFamily(), Response.Status.Family.SUCCESSFUL);

            // retrieve SLA status
            String slaStatus = slaManagerTarget
                    .path("/slas/{slaId}/status")
                    .resolveTemplate("slaId", slaId)
                    .request()
                    .get(String.class);
            System.out.println("SLA status: " + slaStatus);

            // create planning activity
            System.out.println("Creating planning activity for the SLA " + slaId);
            Map<String, String> planningActivityInput = new HashMap<String, String>();
            planningActivityInput.put("sla_id", slaId);
            PlanningActivity planningActivity = planningTarget
                    .path("/plan-activities")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(planningActivityInput), PlanningActivity.class);

            System.out.println("Planning activity created:");
            System.out.println(JsonDumper.dump(planningActivity));
            System.out.println();

            // wait until the planning activity finishes
            System.out.println("Waiting until the planning activity finishes...");
            Date startTime = new Date();
            while (true) {
                // retrieve planning activity status
                Map<String, String> statusResponse = planningTarget
                        .path("/plan-activities/{planActId}/status")
                        .resolveTemplate("planActId", planningActivity.getId())
                        .request(MediaType.APPLICATION_JSON)
                        .get(new GenericType<Map<String, String>>() {
                        });
                PlanningActivity.Status planningActivityStatus = PlanningActivity.Status.valueOf(
                        statusResponse.get("status"));
                System.out.println("Planning activity status: " + planningActivityStatus);

                if (planningActivityStatus == PlanningActivity.Status.ERROR) {
                    planningActivity = planningTarget
                            .path("/plan-activities/{planActId}")
                            .resolveTemplate("planActId", planningActivity.getId())
                            .request(MediaType.APPLICATION_JSON)
                            .get(PlanningActivity.class);
                    String errorMsg = "N/A";
                    for (Annotation annotation : planningActivity.getAnnotations()) {
                        if (annotation.getName().equals("error")) {
                            errorMsg = annotation.getValue();
                            break;
                        }
                    }
                    throw new Exception("Planning activity failed: " + errorMsg);

                } else if (planningActivityStatus == PlanningActivity.Status.ACTIVE) {
                    System.out.println(String.format("Planning activity finished successfully in %f seconds.",
                            (new Date().getTime() - startTime.getTime()) / 1000.0));
                    System.out.println();
                    break;

                } else {
                    Thread.sleep(10000);
                }

                if (new Date().getTime() - startTime.getTime() > PLANNING_ACTIVITY_TIMEOUT * 1000) {
                    throw new Exception("Timeout waiting for the planning activity to finish.");
                }
            }

            // retrieve planning activity
            System.out.println("Planning activity:");
            planningActivity = planningTarget
                    .path("/plan-activities/{planActId}")
                    .resolveTemplate("planActId", planningActivity.getId())
                    .request(MediaType.APPLICATION_JSON)
                    .get(PlanningActivity.class);
            System.out.println(JsonDumper.dump(planningActivity));
            System.out.println();

            System.out.println("Implementation plan:");
            ImplementationPlan implPlan = implementationTarget
                    .path("/impl-plans/{implPlanId}")
                    .resolveTemplate("implPlanId", planningActivity.getActivePlanId())
                    .request(MediaType.APPLICATION_JSON)
                    .get(ImplementationPlan.class);
            System.out.println(JsonDumper.dump(implPlan));
            System.out.println();

            // retrieve implementation activity
            System.out.println("Implementation activity:");
            ImplActivity implActivity = implementationTarget
                    .path("/impl-activities/{implActId}")
                    .resolveTemplate("implActId", planningActivity.getImplActivityId())
                    .request(MediaType.APPLICATION_JSON)
                    .get(ImplActivity.class);
            System.out.println(JsonDumper.dump(implActivity));
            System.out.println();

            // check Monipoli rules
            String monipoliRules = monipoliTarget
                    .path("/monipoli")
                    .request()
                    .get(String.class);
            System.out.println("Monipoli rules:");
            System.out.println(monipoliRules);
            System.out.println();
            String specsWebpoolM1Rule =
                    "specs_webpool_M1" + "\n" +
                            "level_of_redundancy_m1" + "\n" +
                            "1" + "\n" +
                            "geq" + "\n" +
                            sla.getName();
            assertThat(monipoliRules, containsString(specsWebpoolM1Rule));
            String specsWebpoolM2Rule =
                    "specs_webpool_M2" + "\n" +
                            "level_of_diversity_m2" + "\n" +
                            "1" + "\n" +
                            "geq" + "\n" +
                            sla.getName();
            assertThat(monipoliRules, containsString(specsWebpoolM2Rule));

            // delete planninig activity, terminate VM
            System.out.println("Deleting planning activity...");
            planningTarget
                    .path("/plan-activities/{planActId}")
                    .resolveTemplate("planActId", planningActivity.getId())
                    .request()
                    .delete();

            System.out.println("Integration scenario Core AB3 test finished successfully.");

        } catch (WebApplicationException e) {
            String resp = e.getResponse().readEntity(String.class);
            System.out.println("REST API request failed: " + e.getMessage() + "\n" + resp);
            throw e;
        }
    }
}
